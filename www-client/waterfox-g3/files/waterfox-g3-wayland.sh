#!/bin/sh

#
# Run Waterfox G3 under Wayland
#
export MOZ_ENABLE_WAYLAND=1
exec @PREFIX@/bin/waterfox-g3 "$@"
