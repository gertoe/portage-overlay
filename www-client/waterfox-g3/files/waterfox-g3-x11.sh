#!/bin/sh

#
# Run Waterfox G3 on X11
#
export MOZ_DISABLE_WAYLAND=1
exec @PREFIX@/bin/waterfox-g3 "$@"
