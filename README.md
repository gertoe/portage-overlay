# gertoe Portage Overlay

This is my personal portage overlay containing backups of (already deleted)
upstream or other overlays' ebuilds as well as custom-made and custom-patched
ebuilds required for my production environments.

This overlay, however, is, at the date of publishing, the only one offering an
ebuild for Alex Kontos' [Waterfox](https://waterfox.net) (Current and G3) web browsers.
The ebuilds for [www-client/waterfox-current](www-client/waterfox-current) and
[www-client/waterfox-g3](www-client/waterfox-g3) are based on the upstream Mozilla
Firefox ESR ebuild and the www-client/waterfox-classic ebuild provided by the
[poly-c overlay](https://www.gentoofan.org/gentoo/poly-c_overlay/).
