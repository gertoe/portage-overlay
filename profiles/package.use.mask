# Keep python2 only for backward compatibility
dev-python/nuitka python_targets_python2_7
# mask system-libvpx while waterfox-current is still based on Firefox 68esr
www-client/waterfox-current system-libvpx
<mail-client/thunderbird-69.0 system-libvpx system-av1 system-harfbuzz system-icu system-jpeg system-webp system-sqlite system-libevent
