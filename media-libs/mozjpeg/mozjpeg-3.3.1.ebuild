# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Improved JPEG encoder based on libjpeg-turbo"
HOMEPAGE="https://github.com/mozilla/mozjpeg"
SRC_URI="https://github.com/mozilla/${PN}/archive/v${PV}.tar.gz"

LICENSE="BSD IJG"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="sys-libs/zlib"
DEPEND="${RDEPEND}"

S=${WORKDIR}/${PN}-${PV}

src_prepare() {
	# Remove problematic LDFLAGS declaration
#	sed -i -e '/^LDFLAGS/d' src/Makefile.am || die

	# Rerun autotools
	einfo "Regenerating autotools files..."
	WANT_AUTOCONF=2.56 eautoconf
	WANT_AUTOMAKE=1.7 eautomake

	eapply_user
}

src_configure() {
	autoreconf -fiv
	econf
}

src_compile() {
	emake
}


src_install() {
	# wrapper to use renamed libjpeg.so (allows coexistence with libjpeg-turbo)
	echo -e '#!/bin/sh\nLD_PRELOAD="libmozjpeg.so $LD_PRELOAD" .$(basename $0) "$@"' > wrapper
	newbin wrapper mozcjpeg
	newbin wrapper mozjpegtran

	newbin .libs/cjpeg .mozcjpeg
	newbin .libs/jpegtran .mozjpegtran
	newlib.so .libs/libjpeg.so.62.2.0 libmozjpeg.so
	dodoc README.md README-mozilla.txt usage.txt wizard.txt
}
