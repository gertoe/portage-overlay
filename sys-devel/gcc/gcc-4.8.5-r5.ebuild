# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

TOOLCHAIN_PATCH_DEV="vapier"
PATCH_VER="1.5"
UCLIBC_VER="1.0"

# Hardened gcc 4 stuff
PIE_VER="0.6.2"
SPECS_VER="0.2.0"
SPECS_GCC_VER="4.4.3"
# arch/libc configurations known to be stable with {PIE,SSP}-by-default
PIE_GLIBC_STABLE="x86 amd64 mips ppc ppc64 arm ia64"
PIE_UCLIBC_STABLE="x86 arm amd64 mips ppc ppc64"
SSP_STABLE="amd64 x86 mips ppc ppc64 arm"
# uclibc need tls and nptl support for SSP support
# uclibc need to be >= 0.9.33
SSP_UCLIBC_STABLE="x86 amd64 mips ppc ppc64 arm"
#end Hardened stuff

inherit toolchain-legacy

KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sparc ~x86"

RDEPEND=""
DEPEND="${RDEPEND}
	elibc_glibc? ( >=sys-libs/glibc-2.8 )
	>=${CATEGORY}/binutils-2.20"

if [[ ${CATEGORY} != cross-* ]] ; then
	PDEPEND="${PDEPEND} elibc_glibc? ( >=sys-libs/glibc-2.8 )"
fi

src_prepare() {
	if has_version '<sys-libs/glibc-2.12' ; then
		ewarn "Your host glibc is too old; disabling automatic fortify."
		ewarn "Please rebuild gcc after upgrading to >=glibc-2.12 #362315"
		EPATCH_EXCLUDE+=" 10_all_default-fortify-source.patch"
	fi

	einfo "Fix patches for newer toolchain."
	sed -i -e "s/--- gcc\//--- a\/gcc\//" -e "s/+++ gcc\//+++ b\/gcc\//" \
		"${WORKDIR}/piepatch/02_all_gcc48_config.in.patch" \
		"${WORKDIR}/piepatch/05_all_gcc48_gcc.c.patch" \
		"${WORKDIR}/piepatch/20_all_gcc46_config_crtbeginp.patch" \
		"${WORKDIR}/piepatch/24_all_gcc48_invoke.texi.patch" || die

	toolchain-legacy_src_prepare

	einfo "Fixing config/esp.h path."
	sed -i -e "s/config\/esp.h/..\/config\/esp.h/" "${S}/gcc/gcc.c" || die

	use vanilla && return 0
	# Use -r1 for newer piepatchset that use DRIVER_SELF_SPECS for the hardened specs.
	[[ ${CHOST} == ${CTARGET} ]] && eapply "${FILESDIR}"/gcc-spec-env-r1.patch
}
