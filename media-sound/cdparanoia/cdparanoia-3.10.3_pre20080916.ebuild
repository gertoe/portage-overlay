# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit subversion autotools libtool toolchain-funcs multilib-minimal

ESVN_REPO_URI=https://svn.xiph.org/trunk/cdparanoia/

MY_P="${PN}-III-$(ver_cut 2-3)"
DESCRIPTION="An advanced CDDA reader with error correction"
HOMEPAGE="https://www.xiph.org/paranoia"
#SRC_URI="https://downloads.xiph.org/releases/${PN}/${MY_P}.src.tgz
SRC_URI="https://dev.gentoo.org/~pacho/${PN}/${PN}-3.10.2-patches.tar.xz"

LICENSE="GPL-2 LGPL-2.1"
SLOT="0"
KEYWORDS=""
IUSE="static-libs"

RDEPEND="app-eselect/eselect-cdparanoia"
DEPEND="${RDEPEND}"
BDEPEND=""

S="${WORKDIR}/${MY_P}"

src_prepare() {
	default
	mkdir -pv "${WORKDIR}"/patches

	# Patches from previus patchset + Fedora + Debian
	#sed -i -e 'III-10.2/III-10.3/g' "${WORKDIR}"/patches/*.patch
	#eapply "${WORKDIR}"/patches/*.patch

	eapply "${FILESDIR}"/${PN}-missing-sys_types_h.patch #713740

	mv configure.guess config.guess
	mv configure.sub config.sub

	sed -i -e '/configure.\(guess\|sub\)/d' configure.in || die

	mv configure.{in,ac} || die
	eautoconf
	elibtoolize

	multilib_copy_sources
}

multilib_src_configure() {
	tc-export AR CC RANLIB
	econf
}

multilib_src_compile() {
	emake OPT="${CFLAGS} -I${S}/interface"
	use static-libs && emake lib OPT="${CFLAGS} -I${S}/interface"
}

multilib_src_install_all() {
	einstalldocs
	mv "${ED}"/usr/bin/${PN}{,-paranoia}
}

pkg_postinst() {
	eselect ${PN} update ifunset
}

pkg_postrm() {
	eselect ${PN} update ifunset
}
