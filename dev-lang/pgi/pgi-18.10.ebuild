# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="PGI compiler suite"
HOMEPAGE="http://www.pgroup.com/"
MY_PV="20$(ver_cut 1)-$(ver_rs 1- '')"
SRC_URI="pgilinux-${MY_PV}-x86-64.tar.gz"
#SRC_URI="pgilinux-2018-1810-x86-64.tar.gz"

LICENSE="PGI"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="cuda java"

RDEPEND="net-misc/curl"

RESTRICT="mirror strip"

#QA_PREBUILT="
#		opt/pgi/linux86/2018/cuda/4.2/lib/lib*.so.*
#		opt/pgi/linux86-64/18.10/bin/*
#		opt/pgi/linux86-64/18.10/lib/lib*
#		opt/pgi/linux86-64/18.10/lib/*.o
#		opt/pgi/linux86-64/18.10/libso/lib*
#		opt/pgi/linux86-64/18.10/libso/*.o
#		opt/pgi/linux86-64/18.10/cray/lib*
#		opt/pgi/linux86-64/18.10/etc/pgi_license_tool/curl
#		opt/pgi/linux86-64/18.10/REDIST/lib*.so
#		opt/pgi/linux86-64/2018/cuda/5.0/nvvm/cicc
#		opt/pgi/linux86-64/2018/cuda/4.2/nvvm/cicc
#		opt/pgi/linux86-64/2018/acml/5.3.0/lib/lib*
#		opt/pgi/linux86-64/2018/acml/5.3.0/libso/lib*.so
#		opt/pgi/linux86/18.10/etc/pgi_license_tool/curl
#		opt/pgi/linux86/18.10/bin/*
#		opt/pgi/linux86/18.10/lib/lib*
#		opt/pgi/linux86/18.10/lib/*.o
#		opt/pgi/linux86/18.10/libso/lib*
#		opt/pgi/linux86/18.10/cray/lib*
#		opt/pgi/linux86/2018/cuda/5.0/nvvm/cicc
#		opt/pgi/linux86/2018/cuda/4.2/nvvm/cicc
#		opt/pgi/linux86/2018/acml/4.4.0/lib/lib*
#		opt/pgi/linux86/2018/acml/4.4.0/libso/lib*.so
#"

QA_PREBUILT="
		opt/pgi/linux86-64/18.10/REDIST/lib*.so
		opt/pgi/linux86-64/18.10/bin/*
		opt/pgi/linux86-64/18.10/cray/lib*
		opt/pgi/linux86-64/18.10/etc/pgi_license_tool/curl
		opt/pgi/linux86-64/18.10/lib/*.o
		opt/pgi/linux86-64/18.10/lib/lib*
		opt/pgi/linux86-64/18.10/libso/*.o
		opt/pgi/linux86-64/18.10/libso/lib*
		opt/pgi/linux86-64/2018/acml/5.3.0/lib/lib*
		opt/pgi/linux86-64/2018/acml/5.3.0/libso/lib*.so
		opt/pgi/linux86-64/2018/cuda/4.2/nvvm/cicc
		opt/pgi/linux86-64/2018/cuda/5.0/nvvm/cicc
"

S="${WORKDIR}"

PATCHES=( "${FILESDIR}"/${P}-terminal.patch )

pkg_nofetch() {
	einfo "PGI doesn't provide direct download links. Please download"
	einfo "${ARCHIVE} from ${HOMEPAGE}"
}

src_install() {
	dodir /opt/pgi

	command="accept
"
#1
#${ED}/opt/pgi"

	command="${command}
n"

	if use cuda; then
		command="${command}
y
accept"
	else
		command="${command}
n"
	fi

	if use java; then
		command="${command}

accept"
	else
		command="${command}
no"
	fi

	command="${command}
y
n
n
y
"
	./install <<EOF
${command}
EOF
	# fix problems with PGI's C++ compiler and current glibc:
	cd "${ED}"
	epatch "${FILESDIR}/${PN}-13.5-glibc.patch"

	# java symlink might be broken if useflag is disabled:
	if ! use java; then
		rm opt/pgi/linux86-64/18.10/jre || die
	fi

	# replace PGI's curl with the stock version:
	dodir /opt/pgi/linux86-64/18.10/etc/pgi_license_tool
	dosym ../../../../../../usr/bin/curl /opt/pgi/linux86-64/18.10/etc/pgi_license_tool/curl
	dodir /opt/pgi/linux86/18.10/etc/pgi_license_tool
	dosym ../../../../../../usr/bin/curl /opt/pgi/linux86/18.10/etc/pgi_license_tool/curl
}
