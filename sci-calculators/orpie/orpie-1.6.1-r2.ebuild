# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A fullscreen RPN calculator for the console"
HOMEPAGE="https://github.com/pelzlpj/orpie"
SRC_URI="https://github.com/pelzlpj/${PN}/archive/refs/tags/release-${PV}.tar.gz"

LICENSE="GPL-3"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc man utils"

DEPEND=">=dev-lang/ocaml-4.03
	sys-libs/ncurses:0=
	dev-ml/curses
	dev-ml/camlp5
	dev-ml/gsl-ocaml
	dev-ml/num
	dev-ml/dune"

RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}-release-${PV}"

src_prepare() {
	default
	eapply "${FILESDIR}/${PN}-1.6.1-2018-CODATA.patch"
}

src_compile() {
	emake PREFIX="/" -j1
}

src_install() {
	newbin _build/default/src/orpie/main.exe orpie

	if use man;
	then
		doman doc/orpie.1 doc/orpierc.5
	fi

	insinto /etc/orpie
	doins etc/orpierc

	if use doc;
	then
		HTML_DOCS=( doc/manual.html )
		dodoc doc/manual.pdf ${HTML_DOCS}
	fi

	if use utils;
	then
		newbin _build/default/src/orpie-curses-keys/curses_keys.exe orpie-curses-keys
		if use man;
		then
			doman doc/orpie-curses-keys.1
		fi
	fi
}
